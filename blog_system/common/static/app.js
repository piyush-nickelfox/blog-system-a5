var firebaseConfig = {
    apiKey: "AIzaSyACbtlI7Eeew7LanaKeL6zIAawXdI4sLFQ",
    authDomain: "blog-system-a5.firebaseapp.com",
    projectId: "blog-system-a5",
    storageBucket: "blog-system-a5.appspot.com",
    messagingSenderId: "96955688641",
    appId: "1:96955688641:web:36e8957a0895ec42481640",
    measurementId: "G-360EBM4SKL"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

// Get registration token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
messaging.getToken({ vapidKey: 'BBers51g1F2KsMx9WXKHbAze8Yaxm_cCJgIfy3uWoK9TxjkqOCRVYF5IeS6B9ov0Cl8H_m9RWCoh4G6bpBLEEIA' }).then((currentToken) => {
    if (currentToken) {
        // Send the token to your server and update the UI if necessary
        // ...
    } else {
        // Show permission request UI
        console.log('No registration token available. Request permission to generate one.');
        // ...
    }
}).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // ...
});


messaging.requestPermission()
    .then(function() {
        console.log("Have permission.");
        return messaging.getToken();
    })
    .then(function(token) {
        console.log(token);
    })
    .catch(function(err) {
        console.log("Error Occurred");
    })

//when you on not on the webpage
messaging.onMessage(function(payload) {
    console.log('onMessage recieved: ', payload);
});