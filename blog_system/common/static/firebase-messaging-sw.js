importScripts("https://www.gstatic.com/firebasejs/9.6.6/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/9.6.6/firebase-messaging.js");

var firebaseConfig = {
    apiKey: "AIzaSyACbtlI7Eeew7LanaKeL6zIAawXdI4sLFQ",
    authDomain: "blog-system-a5.firebaseapp.com",
    projectId: "blog-system-a5",
    storageBucket: "blog-system-a5.appspot.com",
    messagingSenderId: "96955688641",
    appId: "1:96955688641:web:36e8957a0895ec42481640",
    measurementId: "G-360EBM4SKL"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();


//when you on not on the webpage and come back you get notification
messaging.setBackgroundMessageHandler(function(payload) {
    const title = 'Hello World'
    const options = {
        body: payload.data.status
    }
    return self.registration.showNotification(title, options);
});