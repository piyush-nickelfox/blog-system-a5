from logging import raiseExceptions
from turtle import title
from unicodedata import category
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import  get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils.decorators import method_decorator
from django.urls import reverse
from django.db.models import Max
from apps.member.models import User
from apps.notification.models import Notifications,Token
from apps.notification.views import send_notification

from .models import Article, Like, Comment , Follow

from django.views import View
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from PIL import Image


#<app>/<model>_<viewtype>.html
class Homepage(TemplateView):
    template_name = 'article/home.html'

    def get(self, request):
        context={}
        searched = request.GET.get('search')
        if searched:
            all_authors = User.objects.filter(name__contains = searched, is_author=True).exclude(id=request.user.id).order_by('-created_at')
            all_articles = Article.objects.filter(title__contains=searched).order_by('-created_at')
            context={
                'title': f'Searchfor={searched}',
                'search': searched,
                'authors': all_authors,
                'articles': all_articles
            }
        else:
            user = self.request.user.id
            context['authors'] = User.objects.filter(is_author=True).exclude(id=user)
            context['articles'] = Article.objects.filter(action='Publish').order_by('-updated_at')

        return render(request, self.template_name, context)


class LikeView(LoginRequiredMixin, View):
    template_name = 'article/liked_article.html'

    def get(self, request, **kwargs):
        context={}
        context['articles'] = Like.objects.filter(user=self.request.user)
        return render(self.request, self.template_name, context)

    def post(self, request, **kwargs):
        article = get_object_or_404(Article, id=self.kwargs['pk'])
        likeArticle, created = Like.objects.get_or_create(article=article, user=self.request.user)
        if not created:
            likeArticle.delete()
        return HttpResponseRedirect(reverse('article-detail', kwargs=kwargs))


@login_required
def commentView(request, **kwargs):
    text = request.POST.get('comment')
    article = get_object_or_404(Article, id=request.POST.get('article_id'))
    comment_count = Comment.objects.filter(article=article, user = request.user).count()
    if comment_count < 3:
        commentArticle = Comment.objects.create(article=article, user=self.request.user, text=text)
        commentArticle.save()
    else:
        messages.success(request, "You already commented multiple times, cannot comment further." )
    return HttpResponseRedirect(reverse('article-detail', kwargs=kwargs))


class MyArticleView(ListView):
    template_name='article/my_articles.html'
    
    def get(self, request):
        context={}
        author = User.objects.get(id=request.user.id)
        context['allArticles'] = Article.objects.filter(author=author)
        return render(request, self.template_name, context)


class AuthorArticlesView(ListView):
    template_name='article/author_articles.html'
    
    def get(self, request, **kwargs):
        context={}
        author = User.objects.get(id = self.kwargs['pk'])
        context['author'] = author.name
        context['allArticles'] = Article.objects.filter(author=author, action='Publish')
        return render(request, self.template_name, context)


class ArticleDetailView(DetailView):
    model = Article
    template_name = 'article/article_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        article = get_object_or_404(Article, id=self.kwargs['pk'])
        if self.request.user.is_authenticated:
            liked = False
            likeArticle = Like.objects.filter(article=article, user=self.request.user)
            if likeArticle.exists():
                liked = True
            context['liked'] = liked

        context['likes'] = Like.no_of_likes(self, article)
        context['comments'] = Comment.objects.filter(article=self.kwargs['pk']).order_by('-created_at')
        return context


class Aboutpage(TemplateView):
    template_name = 'article/about.html'


@method_decorator(login_required, name='dispatch')
class ArticleCreateView(CreateView):
    template_name = 'article/add_article.html'
    model = Article
    fields = ['title','category','description', 'cover_image','action']

    def form_valid(self, form):
        form.instance.author = self.request.user 
        action = form.cleaned_data.get("action") 
        if action=="Publish":
            self.save(form)      
        return super().form_valid(form)

    def save(self, form):
        author = self.request.user
        
        followers = Follow.objects.filter(author=author)

        title = form.cleaned_data.get("title")
        category = form.cleaned_data.get('category')
        text = f"{author} posted new article of category in {category}"

        objs = [
            Notifications(
                sender = author,
                receiver= follower.user,
                title = title,
                text = text,
            )
            for follower in followers
        ]
        Notifications.objects.bulk_create(objs=objs)

        user_list = followers.values('user')
    
        user = Token.objects.filter(user__in=user_list).values('user').annotate(max_c = Max('created_at'))
        token = Token.objects.filter( user__in = user.values('user'), created_at__in = user.values('max_c')).values_list('token', flat=True)
        
        send_notification(list(token), title, text)
       
    
   
class ArticleUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    template_name = 'article/add_article.html'
    model = Article
    fields = ['title','category','description', 'cover_image','action']

    def form_valid(self, form):
        form.instance.author = self.request.user
        action = form.cleaned_data.get("action") 
        if action=="Publish":
            ArticleCreateView.save(self, form)  
        return super().form_valid(form)
        
    def test_func(self):
        article = self.get_object()
        if self.request.user == article.author:
            return True
        return False 


class ArticleDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):

    model = Article
    template_name = 'article/article_delete.html'
    success_url='/'
    
    def test_func(self):
        article = self.get_object()
        if self.request.user == article.author:
            return True
        return False 


class FollowView(LoginRequiredMixin, View):
    template_name = 'article/favourite_authors.html'

    def get(self, request):
        context={}
        context['authors'] = Follow.objects.filter(user = request.user)
        return render(request, self.template_name, context)

    def post(self, request, **kwargs):
        author = get_object_or_404(User, id=request.POST.get('author_id'))
        if author!=request.user:
            followAuthor, followed = Follow.objects.get_or_create(author=author, user=request.user)
            if not followed:
                followAuthor.delete()
        else:
            messages.success(request, 'One cannot follow ownself.')
        return HttpResponseRedirect(reverse('homepage'))
