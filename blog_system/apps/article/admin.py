from django.contrib import admin

# Register your models here.
from .models import Article, Like, Comment, Follow, RequestResponse

class StatsAdmin(admin.ModelAdmin):
    list_display = ('user', 'request_path', 'request_method', 'ip_addr', 'browser_info')

admin.site.register(Article)
admin.site.register(Like)
admin.site.register(Comment)
admin.site.register(Follow)
admin.site.register(RequestResponse, StatsAdmin)
