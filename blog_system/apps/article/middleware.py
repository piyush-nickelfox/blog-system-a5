from .models import RequestResponse
from django.db.models import Count

class ArticleMiddleware:
    
    def __init__(self, get_response):
        self.get_response=get_response

    def __call__(self, request):
        if "admin/" not in request.path:
            self.stats(request)
        response = self.get_response(request)
        return response

    def stats(self, request):
        if request.user.is_authenticated:
            user = f'RegisteredUser-{request.user.name}'
        else:
            user = request.user

        req_res = RequestResponse.objects.create(
            user = user,
            request_path = request.path,
            request_method = request.META['REQUEST_METHOD'],
            browser_info = request.META['HTTP_USER_AGENT'],
            ip_addr = request. META. get('REMOTE_ADDR')
        )
        req_res.save()
        
        #top_3_article = RequestResponse.objects.filter(request_path__startswith="/article_detail/").values('request_path').annotate(count=Count('request_path')).order_by('-count')[:3]
        #print(top_3_article)
