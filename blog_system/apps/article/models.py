from email.policy import default
from tkinter.filedialog import SaveAs
from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify

from apps.member.models import BaseModel, User

class Article(BaseModel):
    SAVE_AS = (
        ("Publish", "Publish"),
        ("Unpublish", "Unpublish"),
        ("Draft", "Draft"),
    )
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=1000)
    category = models.CharField(max_length=1000)
    description = models.TextField()
    cover_image = models.ImageField(default='default.jpg', upload_to="article_cover")
    action = models.CharField(max_length=10, choices=SAVE_AS, default='Publish')
    
    def __str__(self):
        return f'{self.title}'
    
    def slug(self):
        return slugify(self.title)
    
    def get_absolute_url(self):
        return reverse("article-detail", kwargs={'pk': self.pk, 'slug': slugify(self.title)})
    

class Like(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user} likes {self.article}'

    def no_of_likes(self, article):
        likes = Like.objects.filter(article=article)
        return f'{likes.count()} likes'


class Comment(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return self.text

    def no_of_comments(self, article):
        comments = Comment.objects.filter(article=article)
        return f'{ comments.count() } likes'


class Follow(BaseModel):
    user = models.ForeignKey(User, related_name='user', on_delete=models.CASCADE)
    author = models.ForeignKey(User, related_name='author', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user} follows {self.author}'
    
    def get_absolute_url(self):
        return reverse("article-follow", kwargs={"pk": self.pk})


class RequestResponse(models.Model):
    user = models.TextField()
    request_path = models.TextField()
    request_method = models.CharField(max_length=1000)
    ip_addr = models.TextField()
    browser_info = models.TextField()
