from django.urls import path

from .views import (
    Homepage, 
    Aboutpage, 
    ArticleDetailView, 
    ArticleCreateView, 
    ArticleUpdateView, 
    ArticleDeleteView, 
    LikeView,
    MyArticleView,
    AuthorArticlesView,
    commentView,
    FollowView
)

urlpatterns = [
    path('', Homepage.as_view(), name="homepage"),
    path('about', Aboutpage.as_view(), name="aboutpage"),
    
    path('myarticles/', MyArticleView.as_view(), name='my-article'),
    path('<str:name><uuid:pk>/articles/', AuthorArticlesView.as_view(), name='all-article'),

    path('article/add/', ArticleCreateView.as_view(), name="add-article"),
    path('article_detail/<slug:slug>-<uuid:pk>/', ArticleDetailView.as_view(), name="article-detail"), 
    
    path('article_detail/<slug:slug>-<uuid:pk>/update/', ArticleUpdateView.as_view(), name="article-update"), 
    path('article_detail/<slug:slug>-<uuid:pk>/delete/', ArticleDeleteView.as_view(), name="article-delete"), 

    path('like/<slug:slug>-<uuid:pk>/', LikeView.as_view(), name="article-like"),
    path('comment/<slug:slug>-<uuid:pk>/', commentView, name="article-comment"),

    path('follow/<uuid:pk>/', FollowView.as_view(), name="author-follow"), 

    path('likedArticles/', LikeView.as_view(), name="liked-articles"), 
    path('favouriteAuthors/', FollowView.as_view(), name="fav-author"), 
]