from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import User

class UserRegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'name', 'mobile_number')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('name', 'mobile_number','is_author','bio','profile_image')
        
    
