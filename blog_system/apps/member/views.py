
from django.shortcuts import  redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages

from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
# Create your views here.
from .forms import UserRegisterForm, CustomUserChangeForm

from django.contrib.auth.views import PasswordChangeView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from django.views.generic import TemplateView, UpdateView


class RegisterPage(TemplateView):
    template_name = 'member/register.html'

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        context['form'] = UserRegisterForm
        return context

    def post(self, request):
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful." )
            return redirect("homepage")
        else:
            messages.error(request, "Unsuccessful registration. Invalid information.")
   

class LoginPage(TemplateView):
    template_name = 'member/login.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title']="Welcome Back"
        context['form'] = AuthenticationForm
        return context 

    def post(self, request):
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(email=email, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {email}.")
                return redirect("homepage")
            else:
                messages.error(request,"Invalid email or password.")
                return redirect("loginpage")
        else:
            messages.error(request,"Invalid details.")
            return redirect("loginpage")


class PasswordsChangeView(LoginRequiredMixin, PasswordChangeView):
    form_class = PasswordChangeForm
    template_name = "member/changePassword.html"

    def post(self,request):
        user = request.user
        if not user.is_verified:
            user.is_verified=True
            user.save()
        return redirect('homepage')

    
@login_required
def logoutpage(request):
    logout(request)
    messages.success(request,'You were successfully logout.')
    return redirect("homepage")


class ProfilePage(LoginRequiredMixin,UpdateView):
    form_class = CustomUserChangeForm
    template_name = 'member/profile.html'
    success_url= '/'

    def get_object(self):
        return self.request.user

#def emailSending(subject, message, manu_user):
#    email_from = None
#    recipient_list = [manu_user.email,]
#    send_mail( subject, message, email_from, recipient_list )

