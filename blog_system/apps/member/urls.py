from django.urls import path

from .views import  RegisterPage, LoginPage, logoutpage, ProfilePage, PasswordsChangeView
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetCompleteView, PasswordResetConfirmView 


urlpatterns = [
    #path('', Homepage.as_view(), name="homepage"),

    path('login/', LoginPage.as_view(), name="loginpage"),
    path('register/', RegisterPage.as_view(), name="registerpage"),

    path('profile/', ProfilePage.as_view(), name="profilepage"),
    path('password/', PasswordsChangeView.as_view() , name="change_password"),
    path('logout/', logoutpage, name="logoutpage"),

    path('passwordreset', 
        PasswordResetView.as_view( template_name='member/passwordReset.html'), 
        name='password_reset'
    ),
    path('passwordreset/done/', 
        PasswordResetDoneView.as_view( template_name='member/passwordResetDone.html'), 
        name='password_reset_done'
    ),
    path('password-reset-confirm/<uidb64>/<token>', 
        PasswordResetConfirmView.as_view( template_name='member/passwordResetConfirm.html'), 
        name='password_reset_confirm'
    ),
    path('passwordresetcomplete', 
        PasswordResetCompleteView.as_view( template_name='member/passwordResetComplete.html'), 
        name='password_reset_complete'
    ),
]