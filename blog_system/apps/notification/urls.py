from django.urls import include, path
from django.views.generic import TemplateView

from .views import showFirebaseJS, send, send_token, NotificationView

urlpatterns = [
    path('send/', send),
    path('firebase-messaging-sw.js', showFirebaseJS, name="show_firebase_js"),
    path('send_token/', send_token, name="send-token"),
    
    path('notification/', NotificationView.as_view(), name="show-notification"),
    path('notification/', NotificationView.as_view(), name="mark-read"),
]
