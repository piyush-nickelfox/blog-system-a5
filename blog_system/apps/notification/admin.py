from django.contrib import admin

# Register your models here.

from .models import Notifications, Token

class NotifyAdmin(admin.ModelAdmin):
    list_display = ['sender', 'receiver', 'title', 'text', 'is_seen']

class TokenAdmin(admin.ModelAdmin):
    list_display = ['user','token','created_at']

admin.site.register(Notifications, NotifyAdmin)
admin.site.register(Token, TokenAdmin)