from django.db import models

from apps.member.models import User, BaseModel
# Create your models here.


class Notifications(BaseModel):
    sender = models.ForeignKey(User, related_name= "sender", on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name="receiver", on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    text = models.TextField(max_length=1000)
    is_seen = models.BooleanField(default=False)


class Token(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.user} - {self.token}'
   