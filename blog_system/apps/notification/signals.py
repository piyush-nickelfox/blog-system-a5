from django.contrib.auth.signals import user_logged_in, user_logged_out, user_login_failed
from django.dispatch import receiver
from apps.member.models import User

from django.db.models.signals import pre_init, post_init, pre_save, post_save, pre_delete, post_delete

from apps.article.models import Article


#@receiver(user_logged_in, sender=User)
#def login_success(sender, request, user, **kwargs):
#    print("-------------")
#    print("Logged-in Signal")
#    print("Sender: ",sender)
#    print("request: ",request)
#    print("User: ",user)
#    print(f"Kwargs: {kwargs}")
#
#
#user_logged_in.connect(login_success, sender=User)
#
#@receiver(user_logged_out, sender=User)
#def log_out(sender, request, user, **kwargs):
#    print("-------------")
#    print("Logged-out Signal")
#    print("Sender: ",sender)
#    print("request: ",request)
#    print("User: ",user)
#    print(f"Kwargs: {kwargs}")
#
#
#@receiver(pre_save, sender=User)
#def at_start_save(sender, instance, **kwargs):
#    print("-------------")
#    print("Pre Save Signal")
#    print("Sender: ", sender)
#    print("instance: ",instance)
#    print(f"Kwargs: {kwargs}")
#
#

#@receiver(post_save, sender=Article)
#def at_end_save(sender, instance, **kwargs):
#    print("-------------")
#    print("Post Save Signal")
#    print("Sender: ", sender)
#    print("instance: ",instance)
#    print(f"Kwargs: {kwargs}")
#    


#
#@receiver(pre_delete, sender=User)
#def at_start_delete(sender, instance, **kwargs):
#    print("-------------")
#    print("Pre delete Signal")
#    print("Sender: ", sender)
#    print("instance: ",instance)
#    print(f"Kwargs: {kwargs}")
#
#
#@receiver(post_delete, sender=User)
#def at_end_delete(sender, instance, **kwargs):
#    print("-------------")
#    print("Post delete Signal")
#    print("Sender: ", sender)
#    print("instance: ",instance)
#    print(f"Kwargs: {kwargs}")
#
#
#@receiver(pre_init, sender=User)
#def at_start_init(sender, *args, **kwargs):
#    print("-------------")
#    print("Pre Init Signal")
#    print("Sender: ", sender)
#    print(f"args: {args}")
#    print(f"Kwargs: {kwargs}")
#
#
#@receiver(post_init, sender=User)
#def at_end_init(sender, *args, **kwargs):
#    print("-------------")
#    print("Post Init Signal")
#    print("Sender: ", sender)
#    print(f"args: {args}")
#    print(f"Kwargs: {kwargs}")