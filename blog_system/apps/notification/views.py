from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json
from django.views.generic import TemplateView
from .models import Notifications, Token

#as the below file must be global hence added as a function
#firebase-messagin-sw.js
def showFirebaseJS(request):
    data='if( "function" === typeof importScripts) {'\
         'importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js");' \
         'importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js"); ' \
         'var firebaseConfig = {' \
         '        apiKey: "AIzaSyACbtlI7Eeew7LanaKeL6zIAawXdI4sLFQ",' \
         '        authDomain: "blog-system-a5.firebaseapp.com",' \
         '        projectId: "blog-system-a5",' \
         '        storageBucket: "blog-system-a5.appspot.com",' \
         '        messagingSenderId: "96955688641",' \
         '        appId: "1:96955688641:web:36e8957a0895ec42481640",' \
         '        measurementId: "G-360EBM4SK"' \
         ' };' \
         'firebase.initializeApp(firebaseConfig);' \
         'const messaging=firebase.messaging();' \
         'messaging.setBackgroundMessageHandler(function (payload) {' \
         '    console.log(payload);' \
         '    const notification=JSON.parse(payload);' \
         '    const notificationOption={' \
         '        body:notification.body,' \
         '        icon:notification.icon' \
         '    };' \
         '    return self.registration.showNotification(payload.notification.title,notificationOption);' \
         '});}'
    return HttpResponse(data,content_type="text/javascript")


def send_token(request):
    if request.is_ajax and request.method == "POST":
        token = request.POST.get('registration_id')
        data={}
        fcmToken, created = Token.objects.get_or_create(user= request.user, token = token)       
        if not created:
           data={"userToken": "already added"} 
        else:
            data={"userToken": "added"}
        
    return JsonResponse(data)


def send_notification(registration_ids , message_title , message_desc):
    fcm_api = "AAAAFpMCZsE:APA91bGAipDiTrhy4ql943vzjcrROzKjJvEvVe6g0J61JW2A5CEgnqn56tBGBnVxmGmRx-e2V-S-UqC_FV_PO-VHXK1pVSf85zEmTIuUeyLGbQA8OpsBR9eaGDkcgrER7D6QuIqcCJ9g"
    url = "https://fcm.googleapis.com/fcm/send"
    
    headers = {
    "Content-Type":"application/json",
    "Authorization": 'key='+fcm_api}

    payload = {
        "registration_ids" :registration_ids,
        "priority" : "high",
        "notification" : {
            "body" : message_desc,
            "title" : message_title,
            #"image" : "https://i.ytimg.com/vi/m5WUPHRgdOA/hqdefault.jpg?sqp=-oaymwEXCOADEI4CSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLDwz-yjKEdwxvKjwMANGk5BedCOXQ",
            #"icon": "https://yt3.ggpht.com/ytc/AKedOLSMvoy4DeAVkMSAuiuaBdIGKC7a5Ib75bKzKO3jHg=s900-c-k-c0x00ffffff-no-rj",
        }
    }
    result = requests.post(url,  data=json.dumps(payload), headers=headers )
    print(result.json())


def send(request):
    resgistration  = [
        "f2eR3OqPKePs_dd-JIaY6X:APA91bGVq_Tkm7JsZ69sYY34fHzE_OaflmJRwZ8KTwMH86b4C_roBmqs0bYaX21w3WhvGtTYjmvTyBWuTgZslfALsHP9OgIfZAYWfPpk4ElvOCehd8kOZrOHiupTlKtl62cDi4Jy_vxq",
        "fKzKEenlQuL40_EWrhFbAV:APA91bHxViLMBqtrvpaW_Rbf4P7ol0CFC1EYvI6GM8UcaRxbcwJOlXAJN25iLcBhhQAmkSRV6zaN1Xs_IqLrQe_BRlDNLKXUqx7uTnbTnMBwt7uY-UCyAsDqLGx_zjJEyEIPoILlQbLK"
    ]
    send_notification(resgistration , 'Code Keen added a new video' , 'Code Keen new video alert')
    return HttpResponse("sent")


class NotificationView(TemplateView):
    template_name = 'notification/all_notification.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['notifications'] = Notifications.objects.filter(receiver=self.request.user).order_by('-created_at')
        return context
    
    def post(self, request):
        Notifications.objects.filter(receiver=request.user).update(is_seen=True)
        return render(request, self.template_name)

